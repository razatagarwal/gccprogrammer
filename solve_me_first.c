//Solve Me First - Warmup - Algorithms - Hackerrank

#include<stdio.h>

int solvemefirst(int a, int b) {
	return a + b;
}

int main()
{
	int a, b;
	scanf("%d %d", &a, &b);
	printf("%d", solvemefirst(a, b));
	return 0;
}

