//Simple Array Sum - Warmup - Algorithms - Hackerrank

#include<stdio.h>

int main()
{
	int n, sum = 0;
	scanf("%d", &n);
	for(int i = 0; i < n; i++) {
		int element;
		scanf("%d", &element);
		sum += element;
	}
	printf("%d", sum);
	return 0;
}
