//Mode of specific range

#include<stdio.h>

int main()
{
	int n;
	scanf("%d", &n);
	int a[n];
	for(int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	int b[n];
	int mode = 0, high = 0;
	for(int i = 0; i < n; i++) {
		for(int j = i + 1; j < n; j++) {
			if(a[i] == a[j]) {
				mode++;
			}
		}
		b[i] = mode;
		if(mode > high) {
			high = mode;
		}
		mode = 0;
	}
	for(int i = 0; i < n; i++) {
		if(b[i] == high) {
			printf("%d ", a[i]);
		}
	}
	return 0;
}
